#!/bin/bash
# Created: Friday, March 12 2021
cdo -f nc -sellonlatbox,-129,-50,51,24, -random,r16000x5600 template_0.5km_16000x5600.nc
cdo -f nc -sellonlatbox,-129,-50,51,24, -random,r8000x2800 template_1km_8000x2800.nc
cdo -f nc -sellonlatbox,-129,-50,51,24, -random,r4000x1400 template_2km_4000x1400.nc
cdo -f nc -sellonlatbox,-129,-50,51,24, -random,r2000x700 template_4km_2000x700.nc
