#!/usr/bin/env python

# demSatBigCONUSHiRes.nc  5.8 MB,  17556x6000, 129W-50W, 24N – 51N
# demSatCentral_AmericaHR.nc 2.4 MB, 14223x5334, 119W-55W, 9N – 33N
# demSatSouth_AmericaHR.nc 6.7 MB, 11556x14223,  83W-31W,51S-13N


import os
import glob
from netCDF4 import Dataset # netCDF library

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

goeswest = "goes11.2011.031.234515.BAND_02.nc"
goeseast = "goes13.2011.031.234517.BAND_02.nc"

# local_dir = './'
# listing = glob.glob(local_dir + '*nc')

fwest = Dataset(goeswest)
feast = Dataset(goeseast)

# print(fwest.__dict__)

# for dim in fwest.dimensions.values():
#     print(dim)

# for dim in feast.dimensions.values():
#     print(dim)

# print(fwest.variables.values())

print(fwest['lat'][:])
print(fwest['lon'][:])
print(fwest['data'][:])
# 129W-50W, 24N-51N
