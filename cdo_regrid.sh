#!/bin/bash
# Created: Saturday, March 13 2021

cat > mygrid << EOF
gridtype = lonlat
gridsize = 1400000
xsize = 2000
ysize = 700
xname = longitude
xlongname = "longitude"
xunits = "degrees_east"
yname = latitude
ylongname = "latitude"
yunits = "degrees_north"
xfirst = -129
xinc = 0.005
yfirst = 51
yinc = -0.005
EOF
cdo -remapnn,mygrid goes11.2011.031.234515.BAND_02.nc outfile.nc
