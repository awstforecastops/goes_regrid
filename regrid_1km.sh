#!/bin/bash

reskm=${1:-1}
bands=${2:-"1"}

indir="/mnt/satellite/yagnesh/west/2011/"
outdir="/mnt/satellite/yagnesh/west/regrid/"
togrid="/mnt/satellite/yagnesh/${reskm}km.grid"
echo reskm: ${reskm}
echo togrid: ${togrid}
echo band:$band_regex

for f in `ls *BAND_01*`; do
    ifile=${f}
    filename=$(basename -- "$ifile")
    extension="${filename##*.}"
    filename="${filename%.*}"
    regrid_name="conus"

    echo cdo -remapbil,mygrid $ifile ${outdir}/${filename}_${regrid_name}.${extension}
    cdo -remapbil,${togrid} $ifile ${outdir}/${filename}_${regrid_name}.${extension}
done
