#!/bin/bash

reskm=${1:-4}
bands=${2:-"1"}

if [ "x"$2 == "x1" ]; then
    band_regex="BAND_0${bands}"
else
    band_regex="BAND_0{${bands}}"
fi

if [ -z $2 ]; then
    band_regex="BAND_01"
fi

# exit

indir="/mnt/satellite/yagnesh/west/2011/"
outdir="/mnt/satellite/yagnesh/west/regrid/"
togrid="/mnt/satellite/yagnesh/${reskm}km.grid"

echo reskm: ${reskm}
echo togrid: ${togrid}
echo band:$band_regex

for f in `ls *BAND_0{2,3,4,5}*`; do
    ifile=${f}
    filename=$(basename -- "$ifile")
    extension="${filename##*.}"
    filename="${filename%.*}"
    regrid_name="conus"

    echo cdo -remapbil,mygrid $ifile ${outdir}/${filename}_${regrid_name}.${extension}
    cdo -remapbil,${togrid} $ifile ${outdir}/${filename}_${regrid_name}.${extension}
done
